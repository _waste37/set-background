# set-bg

Minimal utility for setting the background in a wlroots compositor.

It basically is a [swaybg](https://github.com/swaywm/swaybg) clone without the cairo dependency.

set-bg is free software, see the LICENSE file for copying permissions. Some of the files included in this program are
licensed under different terms; see the individual files for details.

## installing
The program is not packaged anywhere, thus you need to compile it yourself.
The process is as simple as:

```
$ git clone https://gitlab.com/_waste37/set-bg.git
$ cd set-background
$ meson setup build && meson install -C build 
```

## Dependencies

**Runtime**:
- wayland

**Build time**:
- wayland protocols
- wayland-scanner
- meson
- ninja

**Last but not least**:
- a C compiler
- git (to clone this repository)
