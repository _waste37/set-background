/*
 * set-bg sets the background for a wlroots compositor 
 * Copyright (C) 2023  Tiuna Angelini
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifdef __GCC__  
#define _POSIX_C_SOURCE 200809L
#else
#define _XOPEN_SOURCE 700
#endif  

#include <errno.h>
#include <fcntl.h>
#include <png.h>
#include <single-pixel-buffer-v1-client-protocol.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>
#include <viewporter-client-protocol.h>
#include <wayland-client-protocol.h>
#include <wayland-client.h>
#include <zwlr-layer-shell-unstable-v1-client-protocol.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

static const char *usage = 
"usage: set-bg <COLOR|IMAGE>\n"
"       COLOR must be specified in format #rrggbb\n"
"       IMAGE specified as path\n";

struct colour { uint32_t r, g, b, a; };
typedef uint32_t colour_t;
struct image 
{ 
    uint8_t *pixels; 
    int32_t width, height;
};

struct buffer 
{ 
    uint8_t *data; 
    size_t size;
    struct wl_buffer *wl_buf; 
};

typedef enum { CONF_INVALID = 0, CONF_IMAGE, CONF_COLOUR } config_type;
struct config
{
    config_type type;
    union {
        struct colour colour_u32;
        struct image image_argb;
    } val;
};

struct global_state 
{
    struct wl_display *wl_display;
    struct wl_compositor *wl_compositor;
    struct wl_shm *wl_shm;
    struct zwlr_layer_shell_v1 *zwlr_layer_shell;
    struct wp_viewporter *wp_viewporter;
    struct wp_single_pixel_buffer_manager_v1 *wp_single_pixel_buffer_manager;
    
    struct wl_list outputs;
    struct config *config;

    bool is_running;
};

struct output 
{
    struct wl_list link;
    struct global_state *state;
    
    struct wl_output *wl_output;

    struct wl_surface *wl_surface;
    struct zwlr_layer_surface_v1 *zwlr_layer_surface;

    uint32_t wl_name;
    uint32_t zwlr_serial;
    
    int32_t scale, committed_scale;
    uint16_t width, height, committed_width, committed_height;
    bool needs_ack;
    bool damaged;
};

static bool 
read_image(struct image *image_argb, const char *path)
{
    
    image_argb->pixels = stbi_load(path, &image_argb->width, &image_argb->height, NULL, STBI_rgb_alpha);
    if (image_argb->pixels == NULL) return false;

    uint32_t *pixels = (uint32_t*)image_argb->pixels;
    for (int32_t i = 0; i < image_argb->width * image_argb->height; ++i) {
        pixels[i] = (pixels[i]   & 0xff00ff00)  // rrggbbaa -> 00gg00aa
            | ((pixels[i] << 16) & 0x00ff0000)  // rrggbbaa -> bb000000
            | ((pixels[i] >> 16) & 0x000000ff); // rrggbbaa -> 0000rr00
    }
    printf("loaded image: %s\n", path);
    return true;
}

static inline void 
colour_set(struct colour *c, colour_t color)
{
    c->r = ((color >> 16) & 0xffu) * 0x1010101u;
    c->g = ((color >> 8)  & 0xffu) * 0x1010101u;
    c->b = ((color >> 0)  & 0xffu) * 0x1010101u;
    c->a = 0xffffffffu; /*alpha set to max */
}

static void output_free(struct output *o) {
    if (!o) return;

    wl_list_remove(&o->link);
    if (o->zwlr_layer_surface != 0) {
        zwlr_layer_surface_v1_destroy(o->zwlr_layer_surface);
    }
    if (o->wl_surface != 0) {
        wl_surface_destroy(o->wl_surface);
    }
    wl_output_destroy(o->wl_output);
    free(o);
}

static void 
layer_surface_configure(void *data, struct zwlr_layer_surface_v1 *layer_surface,
        uint32_t serial, uint32_t width, uint32_t height)
{
    struct output *o = data;
    o->width = width;
    o->height = height;
    o->zwlr_serial = serial;
    o->damaged = true;
    o->needs_ack = true;
}

static void 
layer_surface_closed(void *data, struct zwlr_layer_surface_v1 *layer_surface)
{
    output_free(data);
}

static const struct zwlr_layer_surface_v1_listener zwlr_layer_surface_listener = {
    .configure = layer_surface_configure,
    .closed = layer_surface_closed
};

static void
layer_surface_init(struct output *o)
{
    o->wl_surface = wl_compositor_create_surface(o->state->wl_compositor);
    struct wl_region *input_region = wl_compositor_create_region(o->state->wl_compositor);
    wl_surface_set_input_region(o->wl_surface, input_region);
    wl_region_destroy(input_region);

    o->zwlr_layer_surface = zwlr_layer_shell_v1_get_layer_surface(
            o->state->zwlr_layer_shell, o->wl_surface, o->wl_output,
            ZWLR_LAYER_SHELL_V1_LAYER_BACKGROUND, "wallpaper");
    zwlr_layer_surface_v1_set_size(o->zwlr_layer_surface, 0, 0);
    zwlr_layer_surface_v1_set_anchor(o->zwlr_layer_surface,
            ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP 
            | ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM
            | ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT 
            | ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT);
    zwlr_layer_surface_v1_set_exclusive_zone(o->zwlr_layer_surface, -1);
    zwlr_layer_surface_v1_add_listener(o->zwlr_layer_surface, 
            &zwlr_layer_surface_listener, o);
    wl_surface_commit(o->wl_surface);
}

static void output_description(void *data, struct wl_output *output, const char *description) {}

static void  
output_done(void *data, struct wl_output *output) 
{
    struct output *o = data;
    if (o->zwlr_layer_surface == NULL) {
        layer_surface_init(o);
    }
}

static void output_geometry(void *data, struct wl_output *output, 
        int32_t x, int32_t y, int32_t width, int32_t height, 
        int32_t subpixel, const char *make, const char *model, int32_t transform) {}

static void output_mode(void *data, struct wl_output *output, 
        uint32_t flags, int32_t width, int32_t height, int32_t refresh) {}

static void output_name(void *data, struct wl_output *output, const char *name) {}

static void output_scale(void *data, struct wl_output *output, int32_t scale)
{
    struct output *o = data;
    o->scale = scale;
    if (o->state->is_running && o->width > 0 && o->height > 0) {
        o->damaged = true;
    }
}

const struct wl_output_listener output_listener = {
    .description = output_description,
    .done = output_done,
    .geometry = output_geometry,
    .mode = output_mode,
    .name = output_name,
    .scale = output_scale
};

static void 
output_add(struct global_state *state, 
        uint32_t name, struct wl_output *output)
{
    struct output *o = malloc(sizeof (struct output));
    o->wl_name = name;
    o->state = state;
    o->wl_output = output;
    o->zwlr_layer_surface = NULL;
    wl_output_add_listener(output, &output_listener, o);
    wl_list_insert(&state->outputs, &o->link); 
}

static void 
handle_global(void *data, struct wl_registry *registry, 
        uint32_t name, const char *interface, uint32_t version)
{
    struct global_state *state = data;

    if (0 == strcmp(interface, wl_compositor_interface.name)) {
        state->wl_compositor = wl_registry_bind(registry, name, &wl_compositor_interface, 4); 
    } else if (0 == strcmp(interface, wl_shm_interface.name)) {
        state->wl_shm = wl_registry_bind(registry, name, &wl_shm_interface, 1); 
    } else if (0 == strcmp(interface, zwlr_layer_shell_v1_interface.name)) {
        state->zwlr_layer_shell = wl_registry_bind(registry, name, &zwlr_layer_shell_v1_interface, 1);
    } else if (0 == strcmp(interface, wp_viewporter_interface.name)) {
        state->wp_viewporter = wl_registry_bind(registry, name, &wp_viewporter_interface, 1);
    } else if (0 == strcmp(interface, wp_single_pixel_buffer_manager_v1_interface.name)) {
        state->wp_single_pixel_buffer_manager = 
            wl_registry_bind(registry, name, &wp_single_pixel_buffer_manager_v1_interface, 1);
    } else if (0 == strcmp(interface, wl_output_interface.name)){
        output_add(state,  name, wl_registry_bind(registry, name, &wl_output_interface, 4));
    }
}

static void 
handle_global_remove(void *data, struct wl_registry *registry, uint32_t name) 
{
    struct global_state *state = data;
    struct output *o, *tmp;
    wl_list_for_each_safe(o, tmp, &state->outputs, link) {
        if (o->wl_name == name) {
            output_free(o);
        }
    }
}

static const struct wl_registry_listener registry_listener = {
    .global = handle_global,
    .global_remove = handle_global_remove
};


static bool 
buffer_init(struct buffer *buf, struct wl_shm *wl_shm, 
        uint32_t width, uint32_t height, uint32_t format) 
{

    int32_t fd, retries = 100;
    do {
        int32_t t = time(NULL);
        pid_t pid = getpid();
        char name[64];
        snprintf(name, sizeof(name), "/set-background-%d-%d", (uint32_t)pid, (int32_t)t);
        fd = shm_open(name , O_RDWR | O_CREAT | O_EXCL, 0600);
        if (fd >= 0) {
            shm_unlink(name);
            break;
        }
        --retries;
    } while (retries > 0 && errno == EEXIST);
    if (fd < 0) return false;

    buf->size = height * width * 4;
    
    if (ftruncate(fd, buf->size)) {
        close(fd);
        return false;
    }

    buf->data = mmap(NULL, buf->size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    struct wl_shm_pool *pool = wl_shm_create_pool(wl_shm, fd, buf->size);
    buf->wl_buf = wl_shm_pool_create_buffer(pool, 
            0, width, height, width * 4, format);
    wl_shm_pool_destroy(pool);
    close (fd);

    return true;
}

static void 
buffer_destroy(struct buffer *buf)
{
    if (buf->wl_buf) {
        wl_buffer_destroy(buf->wl_buf);
    }
    if (buf->data) {
        munmap(buf->data, buf->size);
    }
}

static void
render_image(struct image *img, struct buffer *buf, uint16_t buf_width, uint16_t buf_height)
{
    uint32_t *data = (uint32_t*)buf->data;
    uint32_t *pixels = (uint32_t*)img->pixels;
    uint32_t x_offset, y_offset, img_width, img_height;

    double img_ratio = (double)img->width/(double)img->height; 
    double buf_ratio = (double)buf_width/(double)buf_height; 

    if (img_ratio < buf_ratio) {
        double h_scale = (double)img->width/(double)buf_width;
        x_offset = 0;
        y_offset = (img->height - buf_height*h_scale) / 2; // 
        img_width = img->width;
        img_height = buf_height * h_scale;
    } else {
        double w_scale = (double)img->height/(double)buf_height;
        y_offset = 0;
        x_offset = (img->width - buf_width*w_scale) / 2; // 
        img_height = img->height;
        img_width = buf_width * w_scale;
    }

    // 10x50 -> 200x100
    // (0,0) -> (0,)
    //  500 / 20 =  
    // (500/ 100/20 - 10) / 2

    for (uint32_t i = 0; i < buf_height; ++i) {
        for (uint32_t j = 0; j < buf_width; ++j) {
            uint32_t y = ((i * img_height) / buf_height) + y_offset;
            uint32_t x = ((j * img_width) / buf_width) + x_offset;
            data[i*buf_width + j] = pixels[y*img->width + x];
        }
    }
}

static void 
frame_callback(struct global_state *state, struct output *o, struct config *conf)
{
    uint16_t buf_width = o->width * o->scale;
    uint16_t buf_height = o->height * o->scale;
    if (o->committed_width == buf_width && o->committed_height == buf_height) {
        if (o->committed_scale != o->scale) {
            wl_surface_set_buffer_scale(o->wl_surface, o->scale);
            wl_surface_commit(o->wl_surface);
            o->committed_scale = o->scale;
        }
        return;
    }

    if (conf->type == CONF_COLOUR) {
        struct wl_buffer *buf = wp_single_pixel_buffer_manager_v1_create_u32_rgba_buffer(
                o->state->wp_single_pixel_buffer_manager, 
                conf->val.colour_u32.r, conf->val.colour_u32.g, 
                conf->val.colour_u32.b, conf->val.colour_u32.a);
        wl_surface_attach(o->wl_surface, buf, 0, 0);
        wl_surface_damage_buffer(o->wl_surface, 0, 0, 1, 1); // INt32_MAX,  INt32_MAX);

        struct wp_viewport *viewport = wp_viewporter_get_viewport(
                o->state->wp_viewporter, o->wl_surface);
        wp_viewport_set_destination(viewport, o->width, o->height);
        wl_surface_commit(o->wl_surface);
        wp_viewport_destroy(viewport);
        wl_buffer_destroy(buf);
        return;
    }
    
    struct buffer buf;
    if (!buffer_init(&buf, state->wl_shm, 
                buf_width, buf_height, WL_SHM_FORMAT_XRGB8888)) {
        return;
    }

    render_image(&conf->val.image_argb, &buf, buf_width, buf_height);

    wl_surface_set_buffer_scale(o->wl_surface, o->scale);
    wl_surface_attach(o->wl_surface, buf.wl_buf, 0, 0);
    wl_surface_damage_buffer(o->wl_surface, 0, 0, INT32_MAX, INT32_MAX);
    wl_surface_commit(o->wl_surface);
    
    buffer_destroy(&buf);

    o->committed_width = buf_width;
    o->committed_height = buf_height;
    o->committed_scale = o->scale;
}

static bool 
parse_colour(struct colour *c, const char *colour_str)
{
    if (colour_str[0] != '#' || strlen(colour_str) != 7) {
        return false;
    }

    char *ptr; 
    colour_t col = (colour_t) strtol(colour_str + 1, &ptr, 16);
    if (*ptr != '\0') { 
        return false;
    }

    colour_set(c, col);
    return true;
}



static void 
parse_argv(struct config *conf, int argc, const char **argv)
{
    conf->type = CONF_INVALID;
    if (argc == 2 && 0 != strcmp(argv[1], "-h")) {
        if (read_image(&conf->val.image_argb, argv[1])) {
            conf->type = CONF_IMAGE;
            return;
        } else if (parse_colour(&conf->val.colour_u32, argv[1])) {
            conf->type = CONF_COLOUR;
            return;
        }
        fprintf(stderr, "I don't know what to do with %s", argv[1]);
    }
    fprintf(stderr, "%s", usage);
}

int 
main(int argc, const char *argv[])
{
    struct config conf;
    parse_argv(&conf, argc, argv); 
    if (conf.type == CONF_INVALID) {
        return -1;
    }

    struct global_state state = {0};
    state.config = &conf;
    wl_list_init(&state.outputs);

    state.wl_display = wl_display_connect(NULL);
    if (0 == state.wl_display) {
        fprintf(stderr, "Unable to connect to wayland server.");
        return 1;
    }

    struct wl_registry *registry = wl_display_get_registry(state.wl_display);
    wl_registry_add_listener(registry, &registry_listener, &state);

    if (wl_display_roundtrip(state.wl_display) < 0) {
        fprintf(stderr, "wl_display_roundtrip failed.");
        return 2;
    }

    if (0 == state.wl_compositor || 0 == state.wp_single_pixel_buffer_manager 
            || 0 == state.wp_viewporter || 0 == state.zwlr_layer_shell) {
        fprintf(stderr, "Wayland interface missing.");
        return 3;
    }
    
    // event loop
    state.is_running = true;
    while (state.is_running && wl_display_dispatch(state.wl_display) != -1) {
        struct output *o; 
        wl_list_for_each(o, &state.outputs, link) {
            if (o->needs_ack) {
                o->needs_ack = false;
                zwlr_layer_surface_v1_ack_configure(
                        o->zwlr_layer_surface, o->zwlr_serial);
            }

            if (o->damaged && !(o->damaged = false)) {
                frame_callback(&state, o, &conf);
            }
        }
    }

    struct output *o, *tmp_o;
    wl_list_for_each_safe(o, tmp_o, &state.outputs, link) {
        output_free(o);
    }

    if (conf.type == CONF_IMAGE) {
        free(conf.val.image_argb.pixels);
    }

    wl_display_disconnect(state.wl_display);
    return 0;
}
